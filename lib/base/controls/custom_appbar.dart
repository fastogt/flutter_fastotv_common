import 'dart:core';

import 'package:flutter/material.dart';

class ChannelPageAppBar extends StatelessWidget {
  final String title;
  final Color? backgroundColor;
  final Color? textColor;
  final List<Widget> actions;
  final void Function()? onExit;

  const ChannelPageAppBar(
      {required this.title,
      this.backgroundColor,
      this.textColor,
      this.actions = const [],
      this.onExit});

  @override
  Widget build(BuildContext context) {
    final Color? textColor = this.textColor ?? Theme.of(context).primaryTextTheme.bodySmall!.color;
    return AppBar(
        actionsIconTheme: IconThemeData(color: textColor),
        leading: IconButton(
            onPressed: () {
              if (onExit != null) {
                onExit!();
              } else {
                Navigator.of(context).pop();
              }
            },
            icon: const Icon(Icons.arrow_back),
            color: textColor),
        actions: actions,
        backgroundColor: backgroundColor ?? Theme.of(context).primaryColor,
        title: Text(title, style: TextStyle(color: textColor)));
  }
}
